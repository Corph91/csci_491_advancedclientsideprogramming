$(document).ready(function(){
  // Let us open our database
  function parse_xml(){
    var candidate_xml = {};
    var get_xml = function () {
      var return_data = null;
      $.ajax({
          'async': false,
          'type': "GET",
          'dataType': 'xml',
          'url': "./xml/candidate_profile.xml",
          'success': function (response) {
              return_data = response;
            }
        });
        return return_data;
    }();
    var xml_data = get_xml;
    xml_data = $(xml_data);
    xml_data.find('candidate_info').find('candidate').each(function(){
      name = $(this).find('name').text();
      state = $(this).find('state').text();
      image = $(this).find('image')[0].childNodes[1].data;
      candidate_xml[name] = {};
      candidate_xml[name].name = name;
      candidate_xml[name].state = state;
      candidate_xml[name].image = image;
    });
    return candidate_xml;
  }

  var db_data = [];
  var full = [];
  candidate_data = {};
  candidate_profiles = parse_xml();
  console.log(candidate_profiles);
  city_data = {};
  var indexedDB = window.indexedDB || window.webkitIndexedDB || window.msIndexedDB;
  var IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange;
  var openCopy = indexedDB && indexedDB.open;
  var IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
  if (IDBTransaction)
  {
      IDBTransaction.READ_WRITE = IDBTransaction.READ_WRITE || 'readwrite';
      IDBTransaction.READ_ONLY = IDBTransaction.READ_ONLY || 'readonly';
  }
  $('input, select').css('pointer-events','none').fadeTo('500','0.1');
  $.when(
      $.ajax({type: "GET",url: "Contributions2016.txt",dataType: "text"})
    ).done(function(data){
        w = new Worker("./js/worker.js");
        w.postMessage({'data': data});
        w.onmessage = function(event) {
          var entry = event.data;
          var request = indexedDB.open('VoterData',3);
          var idb;
          request.onupgradeneeded = function(e)
          {
              // e is an instance of IDBVersionChangeEvent
              idb = e.target.result;
              var transaction = event.target.transaction;
              if (idb.objectStoreNames.length != 0) {
                  idb.deleteObjectStore("data");
              }
              idb.createObjectStore('data', {keyPath: 'data', autoIncrement: true});
          };
          request.onsuccess = function(e)
          {
              var idb = e.target.result;
              var trans = idb.transaction('data', IDBTransaction.READ_WRITE);
              var store = trans.objectStore('data');
              // add
              for(i in entry){
                current = entry[i];
                if((candidate_data[current.cand_nm] == null) && (current.cand_nm != null) && ((current.cand_nm != 'undefined undefined'))){
                  candidate_data[current.cand_nm] = {
                    donated_to: 0,
                    donated_from: {},
                    donated_dates: {},
                    donated_candidates: 0
                  }
                  var option = new Option(current.cand_nm.replace(/["]/g, ""), current.cand_nm.replace(/["]/g, ""));
                  $('#candidate_select').append($(option));
                }
                if((city_data[current.contbr_city] == null) && (current.contbr_city != null) && ((current.contbr_city != 'undefined undefined'))){
                  city_data[current.contbr_city] = current.contbr_city;
                  var option = new Option(current.contbr_city.replace(/["']/g, ""), current.contbr_city.replace(/["']/g, ""));
                  $('#city_select').append($(option));
                }
                if(current["trans_id"] != null){
                  var requestAdd = store.add({
                                              candidate: current["cand_nm"],
                                              amount: current['contb_receipt_amt'],
                                              date: current['contb_receipt_dt'],
                                              city: current['contbr_city']+','+current['contbr_st']+' '+current['contbr_zip']});
                }
              }
          };

        }
        $('#submitDate,#clear, .datepicker, #city, select').css('pointer-events','auto').fadeTo('500','1');
        });

        $('#submitDate').click(function(){
          $('#city, #clear, .datepicker, select').css('pointer-events','none').fadeTo('500','0.1');
          var date_in = new Date($('#selectDate').val());
          if(date_in == "invalid date"){
          }else{
            var request = indexedDB.open('VoterData');
            request.onsuccess = function(event)
            {
              var idb = event.target.result;
              var trans = idb.transaction('data', "readwrite");
              var store = trans.objectStore('data');
              var request = store.getAll();
              request.onsuccess = function(event){
                db_data = event.target.result;
              };
            };
            $('#candidate_profile').html('');
            date_in = String(date_in.toUTCString());
            date_in = date_in.substring(0, date_in.length -13);
            w_four = new Worker("./js/dateworker.js");
            w_four.postMessage({'data': {'candidate_data':candidate_data,'db_data':db_data, 'date': date_in}});
            w_four.onmessage = function(event) {
              candidate_data = event.data;
              for(key in candidate_data){
                if(candidate_data[key].donated_to != 0){
                  temp = key.split(' ')[0].replace(/["']/g,"");
                  for(j in candidate_profiles){
                    if(j.replace(/["']/,'').toLowerCase().includes(temp.toLowerCase())){
                      if($('#'+temp).length == 0){
                        image = candidate_profiles[j].image;
                        state = candidate_profiles[j].state;
                        name = candidate_profiles[j].name;
                        $('#candidate_profile').append('<div id="'+temp+'"></div>');
                        $('#'+temp)
                          .append('<div class="candidate" id="'+temp+'-image"></div>')
                          .append('<div class="candidate" id="'+temp+'-name"><p></p></div>')
                          .append('<div class="candidate" id="'+temp+'-state"><p></p></div>')
                          .append('<div class="candidate" id="'+temp+'-total"><p></p></div>');

                        $('#'+temp+'-image').append(image).find('img').css('width','100px').css('height','100px');
                        $('#'+temp+'-state').find('p').text(state);
                        $('#'+temp+'-name').find('p').text(name);
                        $('#'+temp+'-total').find('p').text('$'+Number(candidate_data[key].donated_dates[date_in]).toLocaleString('en-US', {minimumFractionDigits: 2}));
                      }
                    }
                  }
                }
              }
            }
            $('#city, #clear, .datepicker, select').css('pointer-events','auto').fadeTo('500','1');
          }
        });


        $('#city_select').on('change',function(){
          var request = indexedDB.open('VoterData');
          request.onsuccess = function(event)
          {
            var idb = event.target.result;
            var trans = idb.transaction('data', "readwrite");
            var store = trans.objectStore('data');
            var request = store.getAll();
            request.onsuccess = function(event){
              db_data = event.target.result;
              text = $('#city_select').val().replace(/["']/,'');
              $('#candidate_profile').html('');
              w_two = new Worker("./js/cityworker.js");
              w_two.postMessage({'data': {'candidate_data':candidate_data,'db_data':db_data, 'text': text}});
              w_two.onmessage = function(event) {
                candidate_data = event.data;
                for(key in candidate_data){
                  if(candidate_data[key].donated_to != 0){
                    temp = key.split(' ')[0].replace(/["']/g,"");
                    for(j in candidate_profiles){
                      if(j.replace(/["']/,'').toLowerCase().includes(temp.toLowerCase())){
                        if($('#'+temp).length == 0){
                          image = candidate_profiles[j].image;
                          state = candidate_profiles[j].state;
                          name = candidate_profiles[j].name;
                          $('#candidate_profile').append('<div id="'+temp+'"></div>');
                          $('#'+temp)
                            .append('<div class="candidate" id="'+temp+'-image"></div>')
                            .append('<div class="candidate" id="'+temp+'-name"><p></p></div>')
                            .append('<div class="candidate" id="'+temp+'-state"><p></p></div>')
                            .append('<div class="candidate" id="'+temp+'-total"><p></p></div>');

                          $('#'+temp+'-image').append(image).find('img').css('width','100px').css('height','100px');
                          $('#'+temp+'-state').find('p').text(state);
                          $('#'+temp+'-name').find('p').text(name);
                          $('#'+temp+'-total').find('p').text('$'+Number(candidate_data[key].donated_from[text]).toLocaleString('en-US', {minimumFractionDigits: 2}));
                        }
                      }
                    }
                  }
                }
              }
            };
          };
        });

        $('#candidate_select').on('change', function() {
          var request = indexedDB.open('VoterData');
          request.onsuccess = function(event)
          {
            var idb = event.target.result;
            var trans = idb.transaction('data', "readwrite");
            var store = trans.objectStore('data');
            var request = store.getAll();
            request.onsuccess = function(event){
              db_data = event.target.result;
              text = $('#candidate_select').val().replace(/["']/,'');
              search = $('#candidate_select').val();
              w_three = new Worker("./js/candworker.js");
              w_three.postMessage({'data': {'candidate_data':candidate_data,'db_data':db_data, 'text': text}});
              w_three.onmessage = function(event) {
                candidate_data = event.data;
                temp = text.split(' ')[0];
                for(key in candidate_profiles){
                  if(key.replace(/["']/,'').toLowerCase().includes(temp.toLowerCase())){
                    image = candidate_profiles[key].image;
                    state = candidate_profiles[key].state;
                    name = candidate_profiles[key].name;
                    $('#candidate_profile').html('');
                    if($('#'+temp).length == 0){
                      $('#candidate_profile').append('<div class="profile" id="'+temp+'"></div>');
                      $('#'+temp)
                        .append('<div class="candidate" id="'+temp+'-image"></div>')
                        .append('<div class="candidate" id="'+temp+'-name"><p></p></div>')
                        .append('<div class="candidate" id="'+temp+'-state"><p></p></div>')
                        .append('<div class="candidate" id="'+temp+'-total"><p></p></div>').css('margin','auto');
                      $('#'+temp+'-image').append(image).find('img').css('width','100px').css('height','100px');
                      $('#'+temp+'-state').find('p').text(state);
                      $('#'+temp+'-name').find('p').text(name);
                      $('#'+temp+'-total').find('p').text('$'+Number(candidate_data['"'+search+'"'].donated_to).toLocaleString('en-US', {minimumFractionDigits: 2}));
                    }
                  }
                }
              $('.profile').css('padding-left','36.5%').css('padding-right','36.5%');
              w_three.terminate();
              }
            };
          };

        });
        $('#ui-datepicker-div, .datepicker').focusin(function(){
          $('select').hide();
        }).focusout(function(){
          $('select').show();
        });
        $('#clear').click(function(){
          $('#city_select').val('');
          $('#candidate_select').val('');
        });
      });
