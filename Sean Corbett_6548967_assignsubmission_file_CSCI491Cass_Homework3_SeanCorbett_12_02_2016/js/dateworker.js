self.onmessage = function(e){
  var data = e.data.data;
  var candidate_data = data.candidate_data;
  var db_data = data.db_data;
  var date = data.date;
  var selected = '';
  init_calc = true;
  for(entry in candidate_data){
    candidate_data[entry].donated_to = 0;
    if(candidate_data[entry].donated_dates[date] != null){
      init_calc = false;
    }
  }
  if(init_calc){
    for(entry in db_data){
      if(db_data[entry].date == date){
        candidate_data[db_data[entry].candidate].donated_to += parseFloat(db_data[entry].amount);
      }
      if(candidate_data[db_data[entry].candidate].donated_dates[date] == null){
        candidate_data[db_data[entry].candidate].donated_dates[date] = 0;
      }
      candidate_data[db_data[entry].candidate].donated_dates[date] = candidate_data[db_data[entry].candidate].donated_to;
    }
  }else{
    console.log('Bypassed init_calc');
    for(entry in candidate_data){
      candidate_data[entry].donated_to = candidate_data[entry].donated_dates[date];
    }
  }
  this.postMessage(candidate_data);
}
