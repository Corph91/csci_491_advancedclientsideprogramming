self.onmessage = function(e){
  var text = e.data;
  text = text.data.split('\n');
  pushNext = null;
  entries = [];
  for(i in text){
    if(i != 0){
      data = {};
      temp = text[i].split(',');
      temp_date = new Date(temp[12]);
      temp_date = String(temp_date.toUTCString());
      data['cmte_id'] = temp[0];
      data['cand_id'] = temp[1];
      data['cand_nm'] = temp[2] +' '+temp[3];
      data['contbr_nm'] = temp[4] +' '+ temp[5];
      data['contbr_city'] = temp[6];
      data['contbr_st'] = temp[7];
      data['contbr_zip'] = temp[8];
      data['contbr_employer'] = temp[9];
      data['contbr_occupation'] = temp[10];
      data['contb_receipt_amt'] = temp[11];
      data['contb_receipt_dt'] = temp_date.substring(0,temp_date.length - 13);
      data['receipt_desc'] = temp[13];
      data['memo_cd'] = temp[14];
      data['memo_text'] = temp[15];
      data['form_tp'] = temp[16];
      data['file_num'] = temp[17];
      data['trans_id'] = temp[18];
      data['election_tp'] = temp[19];
      entries.push(data);

    }
  }
  this.postMessage(entries);
}
