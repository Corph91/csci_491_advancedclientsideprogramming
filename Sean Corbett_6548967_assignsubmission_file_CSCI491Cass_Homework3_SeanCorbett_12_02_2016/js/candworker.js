self.onmessage = function(e){
  var data = e.data.data;
  var candidate_data = data.candidate_data;
  var db_data = data.db_data;
  var text = data.text;
  var candidate = '';
  var init_calc = true;
  for(entry in candidate_data){
    candidate_data[entry].donated_to = 0;
    if(entry.replace(/["']/g, "").toLowerCase() == text.toLowerCase()){
      if(candidate_data[entry].donated_candidates != 0){
        init_calc = false;
        candidate = entry;
      }
    }
  }
  if(init_calc){
    for( i in db_data){
      if(db_data[i].candidate.replace(/["']/g, "").toLowerCase() == text.toLowerCase().replace(/["']/g, "")){
        if(!isNaN(db_data[i].amount)){
          candidate = db_data[i].candidate;
          candidate_data[db_data[i].candidate].donated_to += parseFloat(db_data[i].amount);
        }
      }
    }
    if(candidate_data[candidate].donated_candidates == 0){
      candidate_data[candidate].donated_candidates = candidate_data[candidate].donated_to;
    }
  }else{
    console.log('Bypassed init_calc');
    candidate_data[candidate].donated_to = candidate_data[candidate].donated_candidates;
  }
  this.postMessage(candidate_data);
}
