self.onmessage = function(e){
  var data = e.data.data;
  var candidate_data = data.candidate_data;
  var db_data = data.db_data;
  var text = data.text;
  var selected = '';
  init_calc = true;
  for(entry in candidate_data){
    candidate_data[entry].donated_to = 0;
    if(candidate_data[entry].donated_from[text] != null){
      init_calc = false;
    }
  }
  if(init_calc){
    for(i=0;i<db_data.length; i++){
      city = db_data[i].city.replace(/["']/g, "").split();
      if(city[0].toLowerCase().includes(text.toLowerCase())){
        if(!isNaN(parseInt(db_data[i].amount))){
          candidate_data[db_data[i].candidate].donated_to += parseFloat(db_data[i].amount);
          if(candidate_data[db_data[i].candidate].donated_from[db_data[i].city.split(',')[0]] == null){
            candidate_data[db_data[i].candidate].donated_from[db_data[i].city.split(',')[0]] = 0;
          }
          candidate_data[db_data[i].candidate].donated_from[db_data[i].city.split(',')[0]] += parseFloat(db_data[i].amount);
        }
      }
    }
  }else{
    for(entry in candidate_data){
      candidate_data[entry].donated_to = candidate_data[entry].donated_from[text];
    }
  }
  this.postMessage(candidate_data);
}
