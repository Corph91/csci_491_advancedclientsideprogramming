$(document).ready(function(){
  $('.non-active > p, .non-active > a').hide()
  $(".card").click(
    function(){
       var $this = $(this);
       if($this.hasClass('active')){
         $this.removeClass('active').addClass('non-active');
         $('p,a',this).slideToggle('fast');
       }
      else{
        $this.removeClass('non-active').addClass('active');
         $('p,a',this).slideToggle('fast');
      }
    }
  );


  $(function() {
      $('a.page-scroll').bind('click', function(event) {
          var $anchor = $(this);
          $('html, body').stop().animate({
              scrollTop: $($anchor.attr('href')).offset().top
          }, 1500, 'easeInOutExpo');
          event.preventDefault();
      });
  });

});
