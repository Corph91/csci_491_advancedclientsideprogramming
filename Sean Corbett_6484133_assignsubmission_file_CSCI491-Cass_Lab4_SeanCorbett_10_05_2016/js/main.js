$(document).ready(function(){
  var generateRand = (function(){
    var min = 1;
    var max = 100;
    return function() {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min)) + min;
    }
  })();

  var generateSeq = (function(num,state){
    seq = this.seq;
    return function(num,state) {
      if(state == "new"){
        seq = [];
        num += 1;
        interval = generateRand();
        init = interval;
        for (i=0; i < num; i++) {
          seq.push(init);
          init += interval;
        }
        return seq;
      }

    }
  })();

var arry = [];

var game = (function(){
  return function() {
    $('input').val('');
    $('h3').show();
    var val = parseInt($(".2-10").val());
    arry = generateSeq(val,"new");
    $('ul').remove();
    $('#seq-list').append('<ul></ul>');
    $('ul').addClass('animated bounceInLeft');
    setTimeout(function(){
      $('ul').removeClass('animated bounceInLeft');
    },1000);
    for(i = 0; i < arry.length-1; i ++){
      if (i < arry.length-2){
        seq_item = $('<li>'+arry[i]+", "+'</li>');
        $('ul').append(seq_item);
      }
      else {
        seq_item = $('<li>'+arry[i]+'</li>');
        $('ul').append(seq_item);
      }
    }
  }
})();

var guess = (function(){
  return function(){
    var val = parseInt($("input").val());
    if (arry.length > 0){
        if(val != arry[(arry.length-1)]){
          $('input').val("Try again!");
          $('ul').addClass('animated shake');
          setTimeout(function(){
            $('ul').removeClass('animated shake');
          },1000);
        }
        else{
          $('input').val("Your guess was correct!");
          $('ul').addClass('animated tada');
          setTimeout(function(){
            $('ul').removeClass('animated tada');
          },1000);
          val = "reset";
          arry = generateSeq(val,"new");
        }
      }
  }
})();

$(function(){
    var $select = $(".2-10");
    for (i=2;i<=10;i++){
        $select.append($('<option></option>').val(i).html(i))
    }
});

  $('#enter').click(function(){
    guess();
  });

  $('#generate-new').click(function(){
    game();
  });

  $('h3').hide();

  $('input').click(function(){
    $('input').val('');
  });
});
