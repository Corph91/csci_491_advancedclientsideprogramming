$(document).ready(function(){
  function iframeLoaded() {
      var iFrameID = document.getElementById('myIframe');
      if(iFrameID) {
            // here you can make the height, I delete it first, then I make it again
            iFrameID.height = "";
            iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
      }
  }
  function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
  }

  $('.contact-form').hide();

  $('.jumbotxt').click(function(){
    $('#name').fadeIn(500);
    $('#email').fadeIn(500);
    $('.contact-form').fadeIn(500);
  });

  $('label').click(function(){
    $('.contact-form').fadeOut(500);
  });

});
