Hey there!

Wow, we're really getting to the end here! This project threw a lot of weird things my way, and the audio thing ended up being a Chrome thing. So
I know I went overboard on the music here, but I figured I'd throw in a drum and bass shmorgasboard. I'm curious what else workers can do. Is there
a maximum on the number of workers an individual application can create? How can one allocate a greater share of bandwidth to a certain worker performing
a more intensive tasks? Is this done automatically? Workers definitely seem powerful.

Happy Thanksgiving Michael! You've made what's been a pretty stressful and hard semester fun and engaging. I've really liked how I've grown as a web
developer throughout this course. When I look at my first pages, the designs were pretty clunky and over the top. As I've continued, I've tried stripping
things down, and I hope that's been for the better. Now go eat some turkey and pie!

-SC