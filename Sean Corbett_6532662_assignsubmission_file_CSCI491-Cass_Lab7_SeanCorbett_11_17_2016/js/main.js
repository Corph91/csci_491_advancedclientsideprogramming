$(document).ready(function(){
  audio_lib = ["./music/jmiller_final_countdown.ogg",
               "./music/enei_and_noel_ft_eastcolors_cracker.ogg",
               "./music/bcuk_nomad.ogg","./music/camo_krooked_ember.ogg",
               "./music/foreign_concept_tag_team.ogg",
               "./music/fierce_and_break_and_nico_draw.ogg"];
  $.each(audio_lib,function(i){
    string = audio_lib[i];
    string = string.split('./music/');
    string = string[1].split('.');
    string = string[0];
    $('ul').append('<li class="item">'+string+"</li>");
  });

  $('.item').hide();
  $('ul').mouseenter(function(){
    $('.item').slideDown(500);
    setTimeout(function(){

    },1000);
  }).mouseleave(function(){
    $('.item').slideUp(500);
  });
  $('.item').click(function(){
    text = './music/'+$(this).text()+'.ogg';
    $('li').removeClass('active');
    $('li').each(function(){
      $(this).find('i').remove('#selected');
    });
    $(this).addClass('active').append('<i id="selected" class="icon-play-circle"></i>');

    $('audio').trigger('pause');
    setTimeout(function(){
      $('audio').attr('src',text).trigger('play');
    },100);

  });
  $('audio').attr('src',audio_lib[0]);
  $.when($.ajax({type: "GET",url: "unique_artists.txt",dataType: "text"})).done(function(data){
    w = new Worker("./js/worker.js");
    w.postMessage({'data': data});
    w.onmessage = function(event) {
      console.log(event.data);
    }
  });

});
