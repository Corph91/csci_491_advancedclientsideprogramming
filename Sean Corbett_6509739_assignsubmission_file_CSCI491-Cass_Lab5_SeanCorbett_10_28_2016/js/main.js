thisuser = {};
loggedin = false;
validup = false;
users = [];
localcount = 0;

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";";
}

var validate = (function(){
  data = {}
  var Form = document.getElementById('signupform');
  if(Form.checkValidity){
    var elements = document.querySelectorAll("#signupform input");
    for (var i = 0, element; element = elements[i++];) {
      type = element.getAttribute('type');
      if (type == 'name'){
        data["name"] = element.value;
      }
      else if (type == 'email') {
        data["email"] = element.value;
      }
      else if (type == 'password') {
        data["password"] = element.value;
      }
    }
    var user = {name: data["name"], email: data["email"], password: data["password"]};
    var existingEntries = JSON.parse(localStorage.getItem("allUsers"));
    if(existingEntries == null) existingEntries = [];
    // Save allEntries back to local storage
    existingEntries.push(user);
    localStorage.setItem("allUsers", JSON.stringify(existingEntries));
    validup = true;
  }
  return false;
});

var signin = (function(){
  data = []
  var Form = document.getElementById('signinform');
  if(Form.checkValidity){
    var elements = document.querySelectorAll("#signinform input");
    for (var i = 0, element; element = elements[i++];) {
      type = element.getAttribute('type');
      if (type == 'email') {
        data["email"] = element.value;
      }
      else if (type == 'password') {
        data["password"] = element.value;
      }

    }
  }
  checkcookie = getCookie('username');

  var existingEntries = JSON.parse(localStorage.getItem("allUsers"));

  for(user in existingEntries){
    if((data.email == existingEntries[user].email) && (data.password == existingEntries[user].password)){
      if(checkcookie == ""){
        setCookie('username',existingEntries[user].name,'10');
        thisuser["name"] = existingEntries[user].name;
      }else{
        alert("You are already signed in!");
        thisuser["name"] = checkcookie;
      }
      loggedin = true;
    }
  }
  return false;
});

var submitcomment = (function(){
  return false;
});

$(document).ready(function(){

  var notadmin = function(){
    $('.post').each(function(){
      name = $(this).attr('name');
      checkcookie = getCookie('username');
      if(name != checkcookie){
        $(this).find('.deletebtn').hide();
      }
    });
  }

  notadmin();

  function loadComments(){
    $('#comments').empty();
    var existingEntries = JSON.parse(localStorage.getItem("allEntries"));
    if(existingEntries == null) existingEntries = false;
    var id = 0;
    for(entry in existingEntries){
        thisentry = existingEntries[entry];
        fromuser = thisentry.name;
        usertext = thisentry.text;
        id = parseInt(entry) + 1;
        existingEntries[entry].id = id;
        $('#comments').append("<div id="+id+" name="+fromuser+" class='post'><h4>"+fromuser+"</h4><p class='deletebtn'>delete</p><p>"+usertext+"</p>");
      }
    localStorage.setItem("currentCount", JSON.stringify(id));
    notadmin();

  }

  loadComments();

  function autoLogin(){
    var name = getCookie('username');
    if(name != ''){
      thisuser['name'] = name;
      loggedin = true;
      $('#sign-in').val('sign out');
    }
  }

  autoLogin();

  $('.signup').hide();
  $('.signin').hide();
  if(!loggedin){
    $('#comment-header').text("please log in to comment");
    $('#commentform :input').prop("disabled", true);
    $('#commentform').hide();
    $('#hello').text('cinema news');
  }else{
    $('#hello').text('hello:')
    $('#hello').append('<p id="username">'+thisuser.name+'</p>');
  }

  $('.close').click(function(){
    $('.close').addClass('animated flash').css('animation-iteration-count','infinite');
    setTimeout(function(){
      $('.signin, .signup').fadeOut(250,"linear");
      $('.close').removeClass('animated flash').css('animation-iteration-count','none');
      $('.article, .comment-system').delay(500).fadeIn(500,"linear").fadeTo(500,1);
    },500);
  });

  $('#sign-in').click(function(){
    val = $('#sign-in').val();
    if(val == "sign in"){
      $('.article,.comment-system,.signup,#comments').fadeOut(250,"linear");
      $('.signin').delay(500).fadeIn(500,"linear");
    }
    else if (val == "sign out"){
      $('#sign-in').val('sign in')
      $('#comment-header').text("please log in to comment")
      $('#commentform').fadeOut(500);
      $('#commentform :input').prop("disabled", true);
      $('#hello').remove('#username');
      $('#hello').text('cinema news');
      function delete_cookie(name) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      }
      delete_cookie("username");
      loggedin = false;
      loadComments();
      notadmin();
    }
  });

  $('#sign-up-lnk').click(function(){
    $('.signin').fadeOut(250,"linear");
    $('.signup').delay(500).fadeIn(500,"linear");
  });

  $('#signupform').submit(function(){
      $('.signup').fadeOut(250,"linear");
      $('.signin').delay(500).fadeIn(500,"linear");
    validup = false;
  });

  $('#signinform').submit(function(){
    if(loggedin){
      $('.comment-system h3').text("comment:");
      $('.signin').fadeOut(250,"linear");
      $('.article, .comment-system, #commentform, #comments').delay(500).fadeIn(500,"linear").fadeTo(500,1);
      $('#commentform :input').prop("disabled", false);
      $('#commentform :input').css('height','auto');
      $('#commentform label').text(thisuser.name);
      $('#sign-in').val('sign out');
      location.reload();
    }
    else{
      $('#signinform input[type=email],#signinform input[type=password]').val('');
    }
    notadmin();
  });


  $('#commentform').submit(function(){
    if($('#commentform textarea').val().length > 10){

      var commentcounter = JSON.parse(localStorage.getItem("currentCount"));
      if(commentcounter == null) commentcounter = 0;
      commentcounter += 1;
      // Save allEntries back to local storage
      localStorage.setItem("currentCount", JSON.stringify(commentcounter));

      var fromuser = thisuser.name;
      var usertext = $('#commentform textarea').val();
      var comment = {name: fromuser,id:commentcounter, text: usertext};
      var existingEntries = JSON.parse(localStorage.getItem("allEntries"));
      if(existingEntries == null) existingEntries = [];
      // Save allEntries back to local storage
      existingEntries.push(comment);
      localStorage.setItem("allEntries", JSON.stringify(existingEntries));
      loadComments();
      }

    $('#commentform textarea').val('')

  });

    $("#comments").on('click','.deletebtn',function(){
      var commentcounter = JSON.parse(localStorage.getItem("currentCount"));
      $(this).parent().remove();
      id = parseInt($(this).parent().attr('id'));
      id += 1
      first = $('#comments :first').attr('id');
      selectedfirst = false;

      //If we are deleting the first comment true
      if(id == first){
        selectedfirst = true;

      }
      //Modify localStorage memory space as follows
      var existingEntries = JSON.parse(localStorage.getItem("allEntries"));
      //If we are deleting the last comment left:
      if(existingEntries.length == 1){
        existingEntries = null;
        localStorage.setItem('allEntries',existingEntries);
        commentcounter = 0;
        // Save allEntries back to local storage
      }
      //else if we are deleting the first comment;
      else if(selectedfirst){
        arry = existingEntries;
        tempcounter = 1
        for(i=1; i<arry.length; i++){
          arry[i-1] = arry[i];
          $('#'+(tempcounter+1)).attr('id',tempcounter);
          arry[i-1].id = tempcounter;
          tempcounter += 1;

        }
        commentcounter = tempcounter -1;
        arry.length--;
        existingEntries = arry;
        selectedfirst = false;
      }
      else{
        reassign = false;
        loadComments();
        for(i=1; i<existingEntries.length; i++){
            thisentry = existingEntries[i];
            if(thisentry.id == id-1){
              var index = existingEntries.indexOf(thisentry);
              existingEntries.splice(index,1);
            }
          }
      }
      localStorage.setItem("allEntries", JSON.stringify(existingEntries));
      localStorage.setItem("currentCount", JSON.stringify(commentcounter));
      //loadComments();
      loadComments();
  });
});
