$(document).ready(function(){
  
  var arry = ['./abasin.html','./vail.html','./breck.html'];

  $('#abasin').click(function(){
    window.location.replace('./abasin.html');
  });
  $('#vail').click(function(){
    window.location.replace('./vail.html');
  });
  $('#breck').click(function(){
    window.location.replace('./breck.html');
  });
  $('#left').mouseenter(function(){
    $('#left').attr('class','icon-chevron-sign-left');
  }).mouseleave(function(){
    $('#left').attr('class','icon-arrow-left');
  });
  $('#right').mouseenter(function(){
    $('#right').attr('class','icon-chevron-sign-right');
  }).mouseleave(function(){
    $('#right').attr('class','icon-arrow-right');
  });

  $('#left').click(function(){
    if(window.location.href.indexOf("abasin") > -1){
      window.location.replace(arry[2]);
    }
    else if(window.location.href.indexOf("vail") > -1){
      window.location.replace(arry[0]);
    }
    else if(window.location.href.indexOf("breck") > -1){
      window.location.replace(arry[1]);
    }
  });
  $('#right').click(function(){
    if(window.location.href.indexOf("abasin") > -1){
      window.location.replace(arry[1]);
    }
    else if(window.location.href.indexOf("vail") > -1){
      window.location.replace(arry[2]);
    }
    else if(window.location.href.indexOf("breck") > -1){
      window.location.replace(arry[0]);
    }
  });

  $('.icon-home').mouseenter(function(){
    $('.icon-home').addClass('animated flash');
  }).mouseleave(function(){
    $('.icon-home').removeClass('animated flash');
  });

  $('#info-a1, #info-a2, #info-a3, #info-v1, #info-v2, #info-v3, #info-b1, #info-b2, #info-b3').hide();

  $('#a-fact-1, #a-fact-2, #a-fact-3, #v-fact-1, #v-fact-2, #v-fact-3, #b-fact-1, #b-fact-2, #b-fact-3').mouseenter(function(){
    $(this).parent().parent().find('p').fadeIn(500);
  });
  $('#ab1, #ab2, #ab3, #v1, #v2, #v3, #b1, #b2, #b3').mouseleave(function(){
    $(this).parent().find('p').fadeOut(500);
  });
});
