Hey there Michael!

You're going to have to forgive me on the project size. I wanted to use some 4k images of Vail, Arapahoe, and Breck (they're all so beautiful!). I
grew up a Colorado kid, so when it comes to snow and skiing I've always been quite spoiled. Arapahoe has consistently been my favorite though, with its
technical terrain, lack of crowds, and sane(ish) ticket prices. I've done more cross country skiing than downhill though (sinc eI was two and a half, I
only started downhill when I was eleven). Cross-country skiing (especially skate-skiing) instills an incredible lesson, that if you want to get to the
fun (going down) you have to work hard not just to get there, but to maintain your momentum. Skate-skiing is also my favorite because it's a beautiful
application of physics; you constantly have to work against your own intertia, and to do so one must have proper form as much as raw physical power.

The same could be said for this project. It seems simple, but drag your mouse over the little dots on the mountains and you'll see information, elegantly
presented. Likewise, the main "about" for each resort takes up a minimal column to the left, an intentional design choice that allows the user to focus
on the beauty of the photos used for the backgrounds. The main splash screen is as much the same, I wanted to let the mountains speak for themselves on 
this one, with only the most critical information presented (allowing the user to let their heart guide their decision as much as their mind when choosing
which mountain to ski or board).

So, yes. In short, I was super enthusiastic about this assignment because I got to show a hidden little passion of mine, a piece of my personal history
that is all happiness and good things. I love every one of these mountains, and I hope I get to go back to Colorado soon to see them and be with them 
again.

-SC