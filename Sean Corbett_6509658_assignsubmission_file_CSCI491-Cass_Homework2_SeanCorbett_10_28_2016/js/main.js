$(document).ready(function(){
  selected = 0
  points = 0
  shuffle = []
  shuffle2 = []
  clicked = []
  audio_lib = ["http://ocrmirror.org/files/music/remixes/Doom_Army_Worthy_of_Phobos_OC_ReMix.mp3",
    "http://ocrmirror.org/files/music/remixes/Advance_Wars_Unworldly_Invasion_OC_ReMix.mp3",
    "http://ocrmirror.org/files/music/remixes/Bloodborne_Undone_by_the_Blood_OC_ReMix.mp3",
    "http://ocrmirror.org/files/music/remixes/EarthBound_SnowBound_OC_ReMix.mp3",
    "http://ocrmirror.org/files/music/remixes/Command_%26_Conquer_Red_Alert_Mud_Mix_OC_ReMix.mp3",
    "http://www.metroidmetal.com/wordpress/wp-content/uploads/2009/09/metroidmetal_primemenu.mp3"
  ];

  var text = ['One','One','Two','Two','Three','Three','Four','Four','Five','Five','Six','Six'];



  var initassign = function(node){

    var shuffle = (function(array) {
    var tmp, current, top = array.length;
    if(top) while(--top) {
      current = Math.floor(Math.random() * (top + 1));
      tmp = array[current];
      array[current] = array[top];
      array[top] = tmp;
    }
    return array;
  });
    selected = 0;
    points = 0;
    clicked = [];
    $('p').hide();
    $('audio').hide();
    $('div').fadeTo(1000,1);
    $('#status')
        .text('audio memory')
        .removeClass('animated pulse')
        .css('animation-iteration-count','none');
    $('.tile').each(function(){
      $(this).css('pointer-events','auto');
    });


    text = shuffle(text);
    console.log(text);

    for(i=0;i<12;i++){
      $('#'+i).text(text[i]);
    }

    $('p').each(function(node){
      if($(this).text()=="One"){
        $(this).parent().find('source').attr('src',audio_lib[0]);
      }
      else if($(this).text()=="Two"){
        $(this).parent().find('source').attr('src',audio_lib[1]);
      }
      else if($(this).text()=="Three"){
        $(this).parent().find('source').attr('src',audio_lib[2]);
      }
      else if($(this).text()=="Four"){
        $(this).parent().find('source').attr('src',audio_lib[3]);
      }
      else if($(this).text()=="Five"){
        $(this).parent().find('source').attr('src',audio_lib[4]);
      }
      else if($(this).text()=="Six"){
        $(this).parent().find('source').attr('src',audio_lib[5]);
      }

    });
  }
  initassign();
  $('.tile').click(function(){
      $action = false;
      $clicked = $(this);
      $content = $(this).find('p');
      $audio = $(this).find('audio');

      var makeclicks = function(){
        selected += 1;
        $audio.show();
        $clicked.addClass('animated flip');
        if(selected == 1){
          $content.addClass('selected');
          $audio.addClass('playable');
        }
        $('.tile').each(function(){
          active1 = $clicked.attr('id');
          tile = $(this).attr('id')
          if(tile == active1){
            $('#'+tile).find('audio').trigger('play');
          }else{
            $('#'+tile).find('audio').trigger('pause');
          }
        });
        $('#status').text('listening...').addClass('animated flash').css('animation-iteration-count','infinite');
      }

      var checkclicks = function(){
        $('#status')
          .text("let's see...")
          .removeClass('animated flash')
          .css('animation-iteration-count','none')
          .addClass('animated pulse')
          .css('animation-iteration-count','infinite');
        if($content.text() != $('.selected').text()){
          $('.tile').each(function(){
            active1 = $('.selected').parent().attr('id');
            active2 = $clicked.attr('id');
            tile = $(this).attr('id')
            if((tile != active1) && (tile != active2)){
              $('#'+tile).css('pointer-events','none');
            }
            if(tile == active2){
              $('#'+tile).find('audio').trigger('play');
            }else{
              $('#'+tile).find('audio').trigger('pause');
            }
          });
          setTimeout(function(){
            $('#status')
              .text("nope, try again!")
              .removeClass('animated pulse')
              .css('animation-iteration-count','none');
            $('.tile, #status').addClass('animated shake');
            $('.tile').each(function(){
              $(this).css('pointer-events','auto');
            });
            $audio.trigger('pause');
            $content2 = $('.selected');
            $content2.removeClass('selected');
            $audio.fadeOut(500);
            $('.playable').fadeOut(500);
            setTimeout(function(){
              $('.tile, #status').removeClass('animated shake');
              $('.tile').removeClass('animated flip');
              $('audio').trigger('pause');
              selected = 0;
            },1000);
          },10000);
        }

        if ($content.text() == $('.selected').text()) {
            points += 1
            $('.tile').each(function(){
              active1 = $('.selected').parent().attr('id');
              active2 = $clicked.attr('id');
              tile = $(this).attr('id')
              if((tile != active1) && (tile != active2)){
                $('#'+tile).css('pointer-events','none');
              }
              if(tile == active2){
                $('#'+tile).find('audio').trigger('play');
              }else{
                $('#'+tile).find('audio').trigger('pause');
              }
            });
            if(points == 6){
              setTimeout(function(){
                $('#status')
                    .text("you win!")
                    .addClass('animated bounce')
                    .css('animation-iteration-count','infinite');
                    $('.tile').addClass('animated bounce');
                $('audio').trigger('pause');
                setTimeout(function(){
                  initassign();
                },1000);
              },10000);

            }else{
              setTimeout(function(){
                $('#status')
                  .text("you got it!")
                  .removeClass('animated pulse')
                  .css('animation-iteration-count','none')
                  .addClass('animated bounce');
                $('.tile').each(function(){
                  $(this).css('pointer-events','auto');
                });
                $audio.trigger('pause');
                $('.selected').parent().css('pointer-events','none');
                $('audio').removeClass('playable');
                $clicked.css('pointer-events','none');
                $('.selected').parent().fadeTo(1000, 0.2);
                $clicked.fadeTo(1000,0.2);
                $('.selected').removeClass('selected');
                selected = 0
                setTimeout(function(){
                  $('#status').removeClass('animated bounce');
                },1000);
              },10000);
              setTimeout(function(){
                $('.tile').removeClass('animated flip');
              },1000);
            }
          }
        }


      if (($clicked.attr('id') != clicked[(clicked.length-1)])){
        clicked.push($clicked.attr('id'));
        if(selected < 2){
          makeclicks();
        }
        if(selected == 2){
          checkclicks();
        }
        if(selected > 2){
        selected = 0;
        }
      }
  });
});
